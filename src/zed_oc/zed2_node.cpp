#include <ros/ros.h>
#include <zed_oc/zed_oc.hpp>
#include <opencv2/opencv.hpp>
#include <string>
#include <sstream>
#include <algorithm>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <sensor_msgs/distortion_models.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>

namespace
{
    const int32_t PUBLISHER_BUFFER_SIZE = 1;
}

namespace enc = sensor_msgs::image_encodings;
namespace distortion_models = sensor_msgs::distortion_models;

class ZED2Publisher
{
private:
    std::unique_ptr<zed::ZED2> cap;
    zed::video::VideoParams params;
    sensor_msgs::CameraInfo lcam_info;
    sensor_msgs::CameraInfo rcam_info;
    image_transport::CameraPublisher lcam_publisher;
    image_transport::CameraPublisher rcam_publisher;
    image_transport::ImageTransport it;
    cv_bridge::CvImage bridge;
    std::string lcam_topic;
    std::string rcam_topic;
    std::string frame_id;
    int ntries;
    uint8_t fps;

    // Capture Timer
    ros::Timer cap_timer;

public:
    ZED2Publisher(ros::NodeHandle *nh)
        : it(*nh), ntries(5)
    {
        /* todo: set ZED video::VideoParams::verbosity */
        ROS_INFO("Setting Video Parameters");
        _setVideoParams();

        /* Set Publisher Parameters */
        ROS_INFO("Setting Publisher Parameters");
        if(!ros::param::get("~left_image_topic", lcam_topic))
            lcam_topic = "zed2/left/image";
        if(!ros::param::get("~right_image_topic", rcam_topic))
            rcam_topic = "zed2/right/image";

        ROS_INFO("Setting Camera Advertisers");
        lcam_publisher = it.advertiseCamera(lcam_topic, PUBLISHER_BUFFER_SIZE);
        rcam_publisher = it.advertiseCamera(rcam_topic, PUBLISHER_BUFFER_SIZE);

        ROS_INFO("Setting Publish Timer");
        cap_timer = nh->createTimer(ros::Duration(1.0/fps), &ZED2Publisher::publishImage,
            this, false, false);

        /* Declare ZED2 Capture Object */
        ROS_INFO("Instantiating ZED2 Object");
        cap.reset(new zed::ZED2());

        /* Set up CV Bridge */
        if(!ros::param::get("~frame_id", frame_id))
            frame_id = "zed2";
        bridge.encoding = enc::BGR8;
    }

    void init(void)
    {
        ROS_INFO("Initializing Video... ");
        if( !cap->initializeVideo() )
        {
            ROS_ERROR("Cannot open camera video capture");
            throw ros::Exception("Cannot open camera video capture");
        }
        ROS_INFO("initialized");
        ROS_INFO("Getting Calibration Data");
        cap->calibrate();
        ROS_INFO("Loading Camera Info");
        _loadCameraInfo();
        ROS_INFO("Starting Capture Timer");
        cap_timer.start();
        ROS_INFO("Done.");
    }

    void stop(void)
    {
        cap_timer.stop();
        cap.reset(new zed::ZED2());
    }

    void publishImage(const ros::TimerEvent &event)
    {
        if( !cap->isInitialized() )
            throw ros::Exception("Camera has not been initialized");
        
        if(lcam_publisher.getNumSubscribers() <= 0 && rcam_publisher.getNumSubscribers() <= 0) return;

        cv::Mat lframe, rframe;

        // Tries to get Image ntries
        for(int i = 0; i < ntries; i++)
        {
            if(cap->getLastFrame(lframe, rframe))
            {
                bridge.header.stamp = lcam_info.header.stamp 
                    = rcam_info.header.stamp = ros::Time::now();

                lcam_info.width = rcam_info.width = lframe.cols;
                lcam_info.height = rcam_info.height = lframe.rows;
                
                bridge.image = lframe;
                bridge.header.frame_id = frame_id+"_l";
                lcam_publisher.publish(*bridge.toImageMsg() ,lcam_info);
                bridge.image = rframe;
                bridge.header.frame_id = frame_id+"_r";
                rcam_publisher.publish(*bridge.toImageMsg() ,rcam_info);

                return;
            }
        }
        ROS_WARN("Could not get any frame in %d tries", ntries);

    }

private:
    void _loadCameraInfo(void)
    {
        // Esse método tem retornado valores errados
        // int width, height;
        // cap->getFrameSize(width, height);
        // width /= 2;

        // lcam_info.header.frame_id = rcam_info.header.frame_id;

        // lcam_info.width = rcam_info.width = width;
        // lcam_info.height = rcam_info.height = height;

        /* Distorion */
        lcam_info.distortion_model = rcam_info.distortion_model = distortion_models::PLUMB_BOB;
        rcam_info.distortion_model = rcam_info.distortion_model = distortion_models::PLUMB_BOB;

        lcam_info.D.push_back(cap->calibration.l_k1);
        lcam_info.D.push_back(cap->calibration.l_k2);
        lcam_info.D.push_back(cap->calibration.l_p1);
        lcam_info.D.push_back(cap->calibration.l_p2);
        lcam_info.D.push_back(cap->calibration.l_k3);

        rcam_info.D.push_back(cap->calibration.r_k1);
        rcam_info.D.push_back(cap->calibration.r_k2);
        rcam_info.D.push_back(cap->calibration.r_p1);
        rcam_info.D.push_back(cap->calibration.r_p2);
        rcam_info.D.push_back(cap->calibration.r_k3);

        /* Camera Matrix */
        int k = 0;
        for(int i = 0; i < cap->calibration.l_cameraMatrix.rows; i++)
        {
            double *p = cap->calibration.l_cameraMatrix.ptr<double>(i);
            for(int j = 0; j < cap->calibration.l_cameraMatrix.cols; j++)
                lcam_info.K[k++] = p[j];
        }

        k = 0;
        for(int i = 0; i < cap->calibration.r_cameraMatrix.rows; i++)
        {
            double *p = cap->calibration.r_cameraMatrix.ptr<double>(i);
            for(int j = 0; j < cap->calibration.r_cameraMatrix.cols; j++)
                rcam_info.K[k++] = p[j];
        }


        /* Projection/Camera Matrix */
        k = 0;
        for(int i = 0; i < cap->calibration.l_cameraMatrix_rectified.rows; i++)
        {
            double *p = cap->calibration.l_cameraMatrix_rectified.ptr<double>(i);
            for(int j = 0; j < cap->calibration.l_cameraMatrix_rectified.cols; j++)
                lcam_info.P[k++] = p[j];
        }

        k = 0;
        for(int i = 0; i < cap->calibration.r_cameraMatrix_rectified.rows; i++)
        {
            double *p = cap->calibration.r_cameraMatrix_rectified.ptr<double>(i);
            for(int j = 0; j < cap->calibration.r_cameraMatrix_rectified.cols; j++)
                rcam_info.P[k++] = p[j];
        }
    }

    void _setVideoParams(void)
    {
        std::string resolution_str;
        if(!ros::param::get("~resolution", resolution_str))
            resolution_str = "HD1080";
        if(zed::string_resolution.find(resolution_str) == zed::string_resolution.end() 
            || resolution_str == "LAST")
        {
            ROS_WARN("Resolution '%s' does not exists. Possible values are: HD2K, HD1080, HD720, VGA or LAST. Setting resolution to HD2K", resolution_str.c_str());
            resolution_str = "HD2K";
        }
        zed::video::RESOLUTION resolution = zed::string_resolution[resolution_str];

        zed::video::FPS fps_;
        if(!ros::param::get("~fps",(int&)fps_))
            fps_ = zed::video::FPS::FPS_15;
        if(std::find(zed::resolution_fps[resolution].begin(), 
            zed::resolution_fps[resolution].end(), fps_) == zed::resolution_fps[resolution].end())
        {
            std::ostringstream s;
            s << "Resolution '" << resolution_str << "' does not accept the FPS value of " << (int)fps_ << std::endl;
            std::string possible_values = "{";
            for(auto &i : zed::resolution_fps[resolution])
                possible_values += std::to_string((int)i) + ",";
            possible_values.pop_back();
            possible_values += "}";
            s << "Possible FPS values for '" << resolution_str << "' are: " << possible_values << std::endl;
            s << "Setting the closest value to " << (int)fps_ << "... ";

            auto closest = [](std::vector<zed::video::FPS> const & vec, zed::video::FPS value) -> zed::video::FPS
            {
                auto const it = std::lower_bound(vec.begin(), vec.end(), value);
                if (it == vec.end()) { return vec.back(); }
                return *it;
            };

            fps_ = closest(zed::resolution_fps[resolution], fps_);

            s << "new FPS = " << (int)fps_;

            ROS_WARN("%s",s.str().c_str());
        }

        params.res = resolution;
        params.fps = fps_;
        fps = (uint8_t)fps_;
    }

};


int main(int argc, char *argv[])
{
    ros::init(argc, argv, "ZED2_node");
    ros::NodeHandle nh;

    ros::AsyncSpinner spinner(0);
    spinner.start();

    ROS_INFO("Starting ZED2 Node...");
    ZED2Publisher zed2Node(&nh);
    zed2Node.init();

    ros::waitForShutdown();
    zed2Node.stop();

    return 0;
}