#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>

namespace enc = sensor_msgs::image_encodings;

cv_bridge::CvImagePtr g_image = nullptr;
sensor_msgs::CameraInfo g_camera_info;

cv::Mat __resize(const cv::Mat& in, int height)
{
    if (in.size().height <= height)
        return in;
    float yf = float(height) / float(in.size().height);
    cv::Mat im2;
    cv::resize(in, im2, cv::Size(), yf, yf);
    return im2;
}

void ImageCallback(const sensor_msgs::Image::ConstPtr &image)
{
  g_image = cv_bridge::toCvCopy(image, enc::BGR8);
}

void InfoCallback(const sensor_msgs::CameraInfo::ConstPtr &info)
{
  g_camera_info = *info;
}

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "cam_view_node");
    ros::NodeHandle nh;

    std::string image_topic;
    std::string info_topic;
    int window_height = 600;
    if(!ros::param::get("~image_topic", image_topic))
        image_topic = "cv_camera/image_raw";
    if(!ros::param::get("~info_topic", info_topic))
        image_topic = "cv_camera/camera_info";
    ros::param::get("~window_height", window_height);
    
    ros::Subscriber sub_image = nh.subscribe(image_topic, 1, &ImageCallback);
    ros::Subscriber sub_info  = nh.subscribe(info_topic, 1, &InfoCallback);

    ros::Rate r(10.0);

    while(sub_image.getNumPublishers() == 0 || sub_info.getNumPublishers() == 0)
        r.sleep();

    while(g_image == nullptr)
    {
        ros::spinOnce();
        r.sleep();
    }

    bool first_loop = true;
    while(1)
    {
        if(first_loop)
        {
            cv::namedWindow("Frame", cv::WINDOW_AUTOSIZE);
            first_loop = false;
        }

        cv::imshow("Frame", __resize(g_image->image, window_height));
        ros::spinOnce();

        // ----> Keyboard handling
        int key = cv::waitKey( 1 );
        if(key=='q' || key=='Q') // Quit
            break;
        // <---- Keyboard handling
    }

    ros::shutdown();
    return 0;
}