#include <zed_oc/zed_oc.hpp>
#include <opencv2/opencv.hpp>

#include <gtest/gtest.h>

cv::Mat __resize(const cv::Mat& in, int height)
{
    if (in.size().height <= height)
        return in;
    float yf = float(height) / float(in.size().height);
    cv::Mat im2;
    cv::resize(in, im2, cv::Size(), yf, yf);
    return im2;
}

zed::ZED2 cap;

TEST(CameraInitialize, intializeVideo)
{
    if( !cap.initializeVideo() )
    {
        FAIL() << "Cannot open camera video capture";
        return;
    }

    SUCCEED() << "Connected to camera SN " << cap.getSerialNumber();
}

TEST(CameraInitialize, calibration)
{
    cv::String path;

    if( !cap.calibrate(path) )
    {
        ADD_FAILURE() << "Cannot get calibration file. Check out Internet connection";
    }

    SUCCEED() << "Calibration file: " << path;
}

TEST(CameraInitialize, frame)
{
    unsigned in_loop = 0;
    unsigned out_loop = 0;
    while(out_loop++ < 120)
    {
        cv::Mat left_frame;
        cv::Mat right_frame;

        if(cap.getLastFrame(left_frame, right_frame))
        {
            left_frame = __resize(left_frame, 600);
            right_frame = __resize(right_frame, 600);

            if(in_loop++ == 0)
            {
                    cv::namedWindow("Left Frame", cv::WINDOW_AUTOSIZE);
                    cv::namedWindow("Right Frame", cv::WINDOW_AUTOSIZE);
            }

            cv::imshow("Left Frame", left_frame);
            cv::imshow("Right Frame", right_frame);
        }
    }

    if(in_loop == 0)
    {
        FAIL() << "Cannot get frame image from camera";
    }
    else if(in_loop < 10)
    {
        ADD_FAILURE() << "Camera cannot run at good frequency";
    }
    else
        SUCCEED();

    cap.~ZED2();
}

int main(int argc, char *argv[])
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}